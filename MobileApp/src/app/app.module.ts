import { ErrorHandler, NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';

import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { SocketIoModule } from 'ng-socket-io';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';

import { PipesModule } from './../pipes/pipes.module';
import { ComponentsModule } from './../components/components.module';

import { ChatRoomsProvider } from '../providers/chat-rooms/chat-rooms.service';
import { ChatRoomSocketProvider } from '../providers/chat-room-socket/chat-room-socket';
import { ChatSocketProvider } from '../providers/chat-socket/chat-socket';

@NgModule({
  declarations: [
    MyApp,
    HomePage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    PipesModule,
    HttpClientModule,
    SocketIoModule,
    ComponentsModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ChatRoomsProvider,
    ChatRoomSocketProvider,
    ChatSocketProvider,
  ]
})
export class AppModule {}
