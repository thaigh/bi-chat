import { Injectable } from '@angular/core';

import { Socket } from 'ng-socket-io';

@Injectable()
export class ChatRoomSocketProvider {

  constructor(public socket: Socket) {
    console.log('Hello ChatRoomSocketProvider Provider');
  }

}
