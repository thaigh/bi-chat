import { Socket } from 'ng-socket-io';
import { Injectable } from '@angular/core';


@Injectable()
export class ChatSocketProvider {

  constructor() { }

  makeSocket(rootUrl: string, namespace: string, opts?: any) {
    return new NamespaceSocket(rootUrl, namespace, opts);
  }

}


export class NamespaceSocket extends Socket {
  constructor(rootUrl: string, namespace: string, opts?: any) {
    super({
      url: `${rootUrl}/${namespace}`,
      options: opts
    })
  }
}
