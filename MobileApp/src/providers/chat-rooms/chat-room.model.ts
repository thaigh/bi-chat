export interface IChatMessage {
  text: string;
  time: Date;
}


export interface IChatRoom {
  name: string;
  socketRoomName: string;
  lastMessage: IChatMessage;
}
