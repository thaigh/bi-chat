import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import 'rxjs/add/operator/map';

import { IChatRoom } from './chat-room.model';

@Injectable()
export class ChatRoomsProvider {

  constructor(private _http: HttpClient) { }

  webServerApiUrl = "http://localhost:3000";

  getChatRooms() : Promise<IChatRoom[]> {
    // return new Promise( (resolve, reject) => {
    //   resolve(
    //     [
    //       { name: 'Test Group', lastMessage: { text: "Hello", time: new Date() } },
    //       { name: 'BI Team', lastMessage: { text: "World", time: new Date() } },
    //       { name: 'Ionic Team', lastMessage: { text: "Test", time: new Date() } }
    //     ]
    //   );
    // });

    return this._http
      .get<IChatRoom[]>(`${this.webServerApiUrl}/rooms`)
      .map( chats => {
        // https://stackoverflow.com/a/48323851/2442468
        chats.forEach( c => c.lastMessage.time = new Date(c.lastMessage.time));
        return chats;
      })
      .toPromise();
  }

}
