import { NgModule } from '@angular/core';

import { IonicModule } from 'ionic-angular';

import { ChatRoomListComponent } from './chat-room-list/chat-room-list';
import { HeaderTitleComponent } from './header-title/header-title';

import { PipesModule } from '../pipes/pipes.module';
import { ChatMessageComponent } from './chat-message/chat-message';

@NgModule({
	declarations: [
    ChatRoomListComponent,
    HeaderTitleComponent,
    ChatMessageComponent
  ],
	imports: [PipesModule, IonicModule],
	exports: [
    ChatRoomListComponent,
    HeaderTitleComponent,
    ChatMessageComponent
  ]
})
export class ComponentsModule {}
