import { Component, Input } from '@angular/core';

@Component({
  selector: 'header-title',
  templateUrl: 'header-title.html'
})
export class HeaderTitleComponent {

  @Input('title') title: string;
  constructor() { }
}
