import { Component, Input } from '@angular/core';

import { IChatMessage } from './../../providers/chat-rooms/chat-room.model';

@Component({
  selector: 'chat-message',
  templateUrl: 'chat-message.html'
})
export class ChatMessageComponent {

  @Input('message') message: IChatMessage;

  constructor() { }

}
