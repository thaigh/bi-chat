import { Component, Input, Output, EventEmitter } from '@angular/core';

import { IChatRoom } from './../../providers/chat-rooms/chat-room.model';

@Component({
  selector: 'chat-room-list',
  templateUrl: 'chat-room-list.html'
})
export class ChatRoomListComponent {

  @Input('chatRooms') chatRooms: IChatRoom[]
  @Output('openChatRoom') openRoomEvent: EventEmitter<IChatRoom> = new EventEmitter();

  constructor() { }

  openChat(chatRoom: IChatRoom) {
    this.openRoomEvent.emit(chatRoom);
  }

}
