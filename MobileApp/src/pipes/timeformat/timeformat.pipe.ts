import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'time',
})
export class TimeFormatPipe implements PipeTransform {

  transform(value: Date) : string {
    if (!value) return "";

    var hours = value.getHours();
    var minutes = value.getMinutes();

    return `${hours}:${minutes}`;
  }
}
