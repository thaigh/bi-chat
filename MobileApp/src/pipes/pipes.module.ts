import { NgModule } from '@angular/core';
import { TimeFormatPipe } from './timeformat/timeformat.pipe';
@NgModule({
	declarations: [TimeFormatPipe],
	imports: [],
	exports: [TimeFormatPipe]
})
export class PipesModule {}
