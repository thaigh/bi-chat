import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { ChatRoomsProvider } from '../../providers/chat-rooms/chat-rooms.service';
import { IChatRoom } from './../../providers/chat-rooms/chat-room.model';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  chats: IChatRoom[];
  private pageTitle = "BI Chat";

  constructor(
      public navCtrl: NavController,
      private _chatRoomsService: ChatRoomsProvider)
  {
    this.chats = [];
  }

  ionViewDidEnter() {
    this._chatRoomsService.getChatRooms()
      .then( (c) => this.chats = c )
      .catch( (err) => console.log(err) );
  }

  openChat(chat: IChatRoom) {
    this.navCtrl.push('ChatRoomPage', { room: chat})
  }

}
