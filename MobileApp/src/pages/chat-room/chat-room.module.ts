import { NgModule } from '@angular/core';

import { IonicPageModule } from 'ionic-angular';

import { PipesModule } from './../../pipes/pipes.module';
import { ComponentsModule } from './../../components/components.module';

import { ChatRoomPage } from './chat-room';

@NgModule({
  declarations: [
    ChatRoomPage,
  ],
  imports: [
    IonicPageModule.forChild(ChatRoomPage),
    ComponentsModule,
    PipesModule
  ],
})
export class ChatRoomPageModule {}
