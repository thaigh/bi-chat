import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Socket } from 'ng-socket-io';
import { Observable } from 'rxjs/Observable';

import { HomePage } from './../home/home';
import { IChatRoom } from './../../providers/chat-rooms/chat-room.model';
import { ChatSocketProvider } from '../../providers/chat-socket/chat-socket';

@IonicPage()
@Component({
  selector: 'page-chat-room',
  templateUrl: 'chat-room.html',
})
export class ChatRoomPage {

  chatRoom : IChatRoom;
  messages: any[];
  messageText: string;
  _socket: Socket;

  nickname = "test user";

  rootUrl = 'http://localhost:3000';

  constructor(
      public navCtrl: NavController,
      public navParams: NavParams,
      private _socketProvider: ChatSocketProvider)
  {
    this.chatRoom = navParams.get('room');
  }

  ionViewDidLoad() {

    if(!this.chatRoom) {
      console.log('chat room info is missing');
      this.navCtrl.setRoot(HomePage);
    } else {
      this.connectToChatRoom();
    }
  }

  connectToChatRoom() {
    this._socket = this._socketProvider.makeSocket(this.rootUrl, this.chatRoom.socketRoomName);
    this._socket.connect();

    this.messages = [];
    this.getMessages().subscribe( (message) => {
      this.messages.push(message);
    });
  }

  ionViewWillLeave() {
    if(this._socket)
      this._socket.disconnect();
  }


  // Socket IO Support Methods.
  // Should be refactored into ChatRoomSocketProvider Service class

  getMessages() {
    const observable = new Observable( obs => {
      this._socket.on('directMessage', (data) => {
        obs.next(data);
      })
    });
    return observable;
  }

  sendMessage(messageText) {
    this._socket.emit(
      'clientMessage',
      this.constructMessage(messageText, this.nickname)
    );
    this.messageText = "";
  }

  constructMessage(messageText: string, nickname: string) {
    return { text: messageText, time: new Date(), from: nickname }
  }

}
