const express = require('express');
const http = require('http');
const socketIO = require('socket.io');
const cors = require('cors');

const { ChatRoom } = require('./models/chat-room');
const { ChatMessage } = require('./models/chat-message');
const { SocketChannel, User } = require('./models/socket-channel');


const app = express();
const server = http.createServer(app);
const io = socketIO(server);

const PORT = process.env.PORT || 3000;

app.use(cors());
app.use(express.json())

app.get('/rooms', (req, res) => {
    res.send(openRooms);
});




var openRooms = [
    new ChatRoom('Test Group').withMessage(new ChatMessage("Hello", new Date() )),
    new ChatRoom('BI Team').withMessage(new ChatMessage("World", new Date() )),
    new ChatRoom('Ionic Team').withMessage(new ChatMessage("Test", new Date() ))
];

var socketChannels = [];




app.post('/rooms/open', (req, res) => {
    var newRoom = new ChatRoom(req.body.roomName);

    var existingRoom = openRooms.find( r => r.socketRoomName === newRoom.socketRoomName);
    if(existingRoom) {
        res.status(400);
        return res.send({message: "Room with this name already exists"})
    }

    console.log(`Creating Socket Room with name: ${newRoom.name}`);

    openSocketRoom(newRoom);
    openRooms.push(newRoom);

    res.status(200);
    res.send(newRoom);
});


openRooms.forEach(r => openSocketRoom(r));

function openSocketRoom(newRoom) {
    var roomNameSpace = io.of(newRoom.socketRoomName);
    var socketChannel = new SocketChannel(roomNameSpace, newRoom.socketRoomName);
    
    socketChannels.push(socketChannel);
}



server.listen(PORT, () => {
    console.log(`Server running on port ${PORT}`)
});
