class ChatRoom {
    // name: string;
    // socketRoomName: string;
    // lastMessage: ChatMessage;

    constructor (name) {
        this.name = name;
        this.socketRoomName = this.getSocketRoomName();
        this.lastMessage = null;
    }

    getSocketRoomName() {
        return this.name.toLocaleLowerCase().replace(new RegExp(" ", 'g'), "_");
    }

    setMessageDetails(messageText, time) {
        if(!this.lastMessage) {
            this.lastMessage = {};
        }

        this.lastMessage.text = messageText;
        this.lastMessage.time = time;
    }

    setMessageObject(messageObj) {
        this.setMessageDetails(messageObj.text, messageObj.time);
    }

    // non mutable
    withMessage(messageObj) {
        var newRoom = new ChatRoom(this.name);
        newRoom.setMessageObject(messageObj);
        return newRoom;
    }

}


module.exports = { ChatRoom }