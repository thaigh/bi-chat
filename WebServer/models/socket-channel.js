const uuidv4 = require('uuid/v4');

const { ChatMessage } = require('./chat-message');

class SocketChannel {
    // socketNameSpace: SocketIo.Namespace
    // channelName: string
    // users: User[]

    constructor(socketNameSpace, channelName) {
        this.socketNameSpace = socketNameSpace;
        this.channelName = channelName;

        this.users = [];

        this.socketNameSpace.on(
            'connection',
            (client) => this.onClientConnect(client)
        );
    }

    addUser(user) {
        this.users.push(user);
    }

    removeUser(user) {
        const idx = this.users.indexOf(user.userId);
        this.users = this.users.splice(idx, 1);
    }


    onClientConnect(client) {
        this.logMessageForChannel("User has joined to channel");
        
        this.broadcastMessage('userJoined', 'User has joined');

        var newUser = new User(client, "nickname");
        this.users.push(newUser);
        
        newUser.sendMessageObject(
            'directMessage',
            this.constructMessageFromServer(
                'Welcome to the chat. ' +
                ((this.users.length == 1)
                    ? 'You are the only person in the room'
                    : `There are ${this.users.length - 1} other users in the room`
                ))
        );
    
        client.on('disconnect', () => {
            this.logMessageForChannel("User has left");
            this.removeUser(newUser);
            this.broadcastMessage('userLeft', 'User nas left')
        });
    
        client.on('clientMessage', (message) => {
            console.log('CLIENT: ' + message.text);
    
            this.distributeMessageToUsers('directMessage', message)
        });
    }

    constructMessageFromServer(messageText) {
        return new ChatMessage(messageText).fromUser(this.channelName)
    }

    logMessageForChannel(message) {
        console.log(`Server [${this.channelName}]: ${message}`);
    }

    broadcastMessage(eventName, messageText) {
        var messageObj = this.constructMessageFromServer(messageText);
        this.distributeMessageToUsers(eventName, messageObj);
    }
    
    distributeMessageToUsers(eventName, messageObj) {
        this.users.forEach( u => {
            u.sendMessageObject(eventName, messageObj)
        })
    }
}

class User {
    // clientSocket: SocketIo.Listener
    // nickName: string
    // userId : UUID

    constructor(clientSocket, nickname) {
        this.clientSocket = clientSocket;
        this.nickname = nickname;
        this.userId = uuidv4();
    }

    sendMessage(eventName, message, fromUserName) {
        this.sendMessageObject(
            eventName,
            new ChatMessage(message).fromUser(fromUserName)
        );
    }

    sendMessageObject(eventName, messageObj) {
        this.clientSocket.emit(eventName, messageObj );
    }

}

module.exports = {
    SocketChannel, User
}