class ChatMessage {
    //text: string;
    //time: Date;
    constructor(text, time) {
        this.text = text;
        this.time = time;
    }
    setUserFrom(userFrom) {
        this.from = userFrom;
    }
    // non-mutating
    fromUser(user) {
        var m = new ChatMessage(this.text, this.time);
        m.setUserFrom(user);
        return m;
    }
}

module.exports = {ChatMessage};